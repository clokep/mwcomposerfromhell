mwcomposerfromhell
##################

**mwcomposerfromhell** is a Python package that provides an easy-to-use composer
to convert the MediaWiki wikicode to HTML via `mwparserfromhell`_. It supports
Python 3.

.. _mwparserfromhell: https://github.com/earwig/mwparserfromhell/

Usage
-----

Normal usage is rather straightforward (where ``text`` is page text)::

    >>> import mwparserfromhell
    >>> import mwcomposerfromhell
    >>> wikicode = mwparserfromhell.parse(text)
    >>> html = mwcomposerfromhell.compose(wikicode)

``html`` is a ``str`` object for the output HTML.
